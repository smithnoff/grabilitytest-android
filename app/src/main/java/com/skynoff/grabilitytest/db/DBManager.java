package com.skynoff.grabilitytest.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.skynoff.grabilitytest.background.ws.responses.MovieListResponse;
import com.skynoff.grabilitytest.ui.model.pojos.MovieModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by cesar.smith on 9/30/2017.
 */

public class DBManager {
    //Tables
    public static final String tableMoviePage = "movie_page";
    public static final String tableMovies = "movies";
    public static final String tableUpcoming = "upcoming";


    //tableMoviePage attributes
    public static final String id = "id";
    public static final String createdBy = "createdBy";
    public static final String description = "description";
    public static final String favoriteCount = "favoriteCount";
    public static final String items = "items";
    public static final String itemCount = "itemCount";
    public static final String iso6391 = "iso6391";
    public static final String name = "name";
    public static final String posterPath = "posterPath";

    //tableMovies attributes
    //    public static final String id = "id";
    public static final String pId = "p_id";
    public static final String voteAverage = "vote_average";
    public static final String voteCount = "vote_count";
    public static final String video = "video";
    public static final String mediaType = "media_type";
    public static final String title = "title";
    public static final String popularity = "popularity";
    //    public static final String posterPath = "posterPath";
    public static final String originalLanguage = "original_language";
    public static final String originalTitle = "original_title";
    public static final String genreIds = "genre_ids";
    public static final String backdropPath = "backdrop_path";
    public static final String adult = "adult";
    public static final String overview = "overview";
    public static final String releaseDate = "release_date";


    public static final String createMoviePage = "CREATE TABLE " + tableMoviePage + " ( " + createdBy + " TEXT, " + description + " TEXT, " + favoriteCount + " INTEGER, " + id + " TEXT, " + items + " TEXT, " + itemCount + " INTEGER, " + iso6391 + " TEXT, " + name + " TEXT, " + posterPath + " TEXT, PRIMARY KEY(" + id + ") )";
    public static final String createMovies = "CREATE TABLE " + tableMovies + " ( " + voteAverage + " NUMERIC, " + voteCount + " INTEGER, " + id + " TEXT, " + pId + " TEXT, " + video + " TEXT, " + mediaType + " TEXT, " + title + " TEXT, " + popularity + " NUMERIC, " + posterPath + " TEXT, " + originalLanguage + " TEXT, " + originalTitle + " TEXT, " + genreIds + " TEXT, " + backdropPath + " TEXT, " + adult + " TEXT, " + overview + " TEXT, " + releaseDate + " TEXT, PRIMARY KEY(" + id + ") )";
    public static final String createUpcoming = "CREATE TABLE " + tableUpcoming + " ( " + voteAverage + " NUMERIC, " + voteCount + " INTEGER, " + id + " TEXT, " + pId + " TEXT, " + video + " TEXT, " + mediaType + " TEXT, " + title + " TEXT, " + popularity + " NUMERIC, " + posterPath + " TEXT, " + originalLanguage + " TEXT, " + originalTitle + " TEXT, " + genreIds + " TEXT, " + backdropPath + " TEXT, " + adult + " TEXT, " + overview + " TEXT, " + releaseDate + " TEXT, PRIMARY KEY(" + id + ") )";

    private SQLiteDatabase db;
    private DBHelper dbHelper;


    public DBManager(Context context) {
        dbHelper = new DBHelper(context);
    }

    private void openReadableDB() {
        db = dbHelper.getReadableDatabase();
    }

    private void openWriteableDB() {
        db = dbHelper.getWritableDatabase();
    }

    private void closeDB() {
        if (db != null) {
            db.close();
        }
    }


    //CRUD Methods

    private ContentValues contentValues;



    //-------------------------------------CRUD for Movies -----------------------------------------

    public void addMovies(MovieListResponse movieList) {
        this.openWriteableDB();
        Log.e("tamañolista", movieList.getItemCount().toString());


        setMoviesContainer(movieList);


    }
    private void setMoviesContainer(MovieListResponse movieList) {
        contentValues = new ContentValues();
        List<MovieModel> movies = new ArrayList<>();
        movies = movieList.getItems();

        for (MovieModel movie : movies) {
            contentValues.put(pId, movieList.getId());
            contentValues.put(id, movie.getId());
            contentValues.put(voteAverage, movie.getVoteAverage());
            contentValues.put(voteCount, movie.getVoteCount());
            contentValues.put(video, movie.getVideo());
            contentValues.put(mediaType, movie.getMediaType());
            contentValues.put(title, movie.getTitle());
            contentValues.put(popularity, movie.getPopularity());
            contentValues.put(posterPath, movie.getPosterPath());
            contentValues.put(originalLanguage, movie.getOriginalLanguage());
            contentValues.put(originalTitle, movie.getOriginalTitle());
            contentValues.put(genreIds, movie.getId());
            contentValues.put(backdropPath, movie.getBackdropPath());
            contentValues.put(adult, movie.getAdult());
            contentValues.put(overview, movie.getOverview());
            contentValues.put(releaseDate, movie.getReleaseDate());
            db.insert(tableMovies, null,contentValues);
        }
        this.closeDB();

    }



    public List<MovieModel> getMovies() {
        List<MovieModel> movies = new ArrayList<>();
        this.openReadableDB();
        Cursor cursor = db.query(tableMovies, null, null, null, null, null, popularity);

        if (cursor != null) {
            cursor.moveToFirst();
            Log.e("cursor size: ", "" + cursor.getCount());

            do {
                movies.add(assamblyMovie(cursor));
            } while (cursor.moveToNext());
            cursor.close();

        }
        Collections.sort(movies,MovieModel.getPopular);
        this.closeDB();
        return movies;


    }
    public List<MovieModel> getTopRatedMovies() {
        List<MovieModel> movies = new ArrayList<>();
        this.openReadableDB();
        Cursor cursor = db.query(tableMovies, null, null, null, null, null, voteAverage+" DESC");

        if (cursor != null) {
            cursor.moveToFirst();
            Log.e("cursor size: ", "" + cursor.getCount());

            do {
                movies.add(assamblyMovie(cursor));
            } while (cursor.moveToNext());
            cursor.close();

        }
        this.closeDB();
        return movies;


    }

    private MovieModel assamblyMovie(Cursor cursor) {
        MovieModel movieModel = new MovieModel();
        movieModel.setId(cursor.getInt(cursor.getColumnIndex(id)));
        movieModel.setVoteAverage(cursor.getDouble(cursor.getColumnIndex(voteAverage)));
        movieModel.setVoteCount(cursor.getInt(cursor.getColumnIndex(voteCount)));
        //movieModel.setVideo(cursor.getString(cursor.getColumnIndex(voteCount)));
        movieModel.setMediaType(cursor.getString(cursor.getColumnIndex(mediaType)));
        movieModel.setTitle(cursor.getString(cursor.getColumnIndex(title)));
        movieModel.setPopularity(cursor.getDouble(cursor.getColumnIndex(popularity)));
        movieModel.setPosterPath(cursor.getString(cursor.getColumnIndex(posterPath)));
        movieModel.setOriginalLanguage(cursor.getString(cursor.getColumnIndex(originalLanguage)));
        movieModel.setOriginalTitle(cursor.getString(cursor.getColumnIndex(originalTitle)));
        //movieModel.setGenreIds(cursor.getString(cursor.getColumnIndex(originalTitle)));
        movieModel.setBackdropPath(cursor.getString(cursor.getColumnIndex(backdropPath)));
        //movieModel.setAdult(cursor.getString(cursor.getColumnIndex(backdropPath)));
        movieModel.setOverview(cursor.getString(cursor.getColumnIndex(overview)));
        movieModel.setReleaseDate(cursor.getString(cursor.getColumnIndex(releaseDate)));


        return movieModel;
    }

    public void deleteTableMovies() {
        this.openWriteableDB();
        db.delete(tableMovies, null, null);
        db.delete(tableUpcoming, null, null);


    }
    //-------------------------------------CRUD for Upcomings -----------------------------------------

    public void setUpcomingsContainer(List<MovieModel> movies) {
        contentValues = new ContentValues();
         this.openWriteableDB();

        for (MovieModel movie : movies) {
            contentValues.put(pId, movie.getId());
            contentValues.put(id, movie.getId());
            contentValues.put(voteAverage, movie.getVoteAverage());
            contentValues.put(voteCount, movie.getVoteCount());
            contentValues.put(video, movie.getVideo());
            contentValues.put(mediaType, movie.getMediaType());
            contentValues.put(title, movie.getTitle());
            contentValues.put(popularity, movie.getPopularity());
            contentValues.put(posterPath, movie.getPosterPath());
            contentValues.put(originalLanguage, movie.getOriginalLanguage());
            contentValues.put(originalTitle, movie.getOriginalTitle());
            contentValues.put(genreIds, movie.getId());
            contentValues.put(backdropPath, movie.getBackdropPath());
            contentValues.put(adult, movie.getAdult());
            contentValues.put(overview, movie.getOverview());
            contentValues.put(releaseDate, movie.getReleaseDate());
            db.insert(tableUpcoming, null,contentValues);
        }
        this.closeDB();

    }
    public List<MovieModel> getUpcomingMovies() {
        List<MovieModel> movies = new ArrayList<>();
        this.openReadableDB();
        Cursor cursor = db.query(tableUpcoming, null, null, null, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
            Log.e("cursor size: ", "" + cursor.getCount());

            do {
                movies.add(assamblyMovie(cursor));
            } while (cursor.moveToNext());
            cursor.close();

        }
        this.closeDB();
        return movies;


    }

    public void deleteTableUpcomings() {
        this.openWriteableDB();
        db.delete(tableMovies, null, null);
        db.delete(tableUpcoming, null, null);


    }


}
