package com.skynoff.grabilitytest.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.skynoff.grabilitytest.ui.utils.GlobalVariables;

/**
 * Created by cesar.smith on 9/30/2017.
 */

public class DBHelper extends SQLiteOpenHelper {

    private static final String name= GlobalVariables.DB_NAME;
    private static final int version=1;

    public DBHelper(Context context) {
        super(context, name, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(DBManager.createMoviePage);
        sqLiteDatabase.execSQL(DBManager.createMovies);
        sqLiteDatabase.execSQL(DBManager.createUpcoming);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
