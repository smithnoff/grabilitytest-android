package com.skynoff.grabilitytest.ui.model.implementations;

import android.content.Context;

import com.skynoff.grabilitytest.db.DBManager;
import com.skynoff.grabilitytest.ui.presenter.callbacks.MovieListCallback;


/**
 * Created by cesar.smith on 10/1/2017.
 */

public class MovieListModel implements MovieListCallback.model {
    MovieListCallback.presenter presenter;
   DBManager manager;
    public MovieListModel(MovieListCallback.presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void getPopular(Context context) {
        manager=new DBManager(context);
        presenter.sortByPopularity(manager.getMovies());
    }

    @Override
    public void getTopRated(Context context) {
        manager=new DBManager(context);
        presenter.sortByRating(manager.getTopRatedMovies());
    }

    @Override
    public void getUpcoming(Context context) {
        manager=new DBManager(context);
        presenter.sortByUpcoming(manager.getUpcomingMovies());

    }
}
