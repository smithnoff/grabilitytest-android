package com.skynoff.grabilitytest.ui.presenter.implementations;

import android.content.Context;

import com.skynoff.grabilitytest.ui.model.implementations.MovieListModel;
import com.skynoff.grabilitytest.ui.model.pojos.MovieModel;
import com.skynoff.grabilitytest.ui.presenter.callbacks.MovieListCallback;

import java.util.List;

/**
 * Created by cesar.smith on 10/1/2017.
 */

public class MovieListPresenter implements MovieListCallback.presenter {

    MovieListCallback.view view;
    MovieListCallback.model model;

    public MovieListPresenter(MovieListCallback.view view) {
        this.view = view;
        this.model = new MovieListModel(MovieListPresenter.this);
    }

    @Override
    public void sortByRating(List<MovieModel> list) {
        if (view != null) {
            view.sortByRating(list);
        }
    }

    @Override
    public void sortByPopularity(List<MovieModel> list) {
        if (view != null) {
            view.sortByPopularity(list);

        }
    }

    @Override
    public void sortByUpcoming(List<MovieModel> list) {
        if (view != null) {
            view.sortByUpcoming(list);

        }
    }


    @Override
    public void getUpcoming(Context context) {
        if (view != null) {
         model.getUpcoming(context);
        }
    }

    @Override
    public void getPopular(Context context) {
        if (view != null) {
            model.getPopular(context);
        }
    }

    @Override
    public void getTopRated(Context context) {
        if (view != null) {
            model.getTopRated(context);
        }
    }
}
