package com.skynoff.grabilitytest.ui.view.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.skynoff.grabilitytest.R;
import com.skynoff.grabilitytest.ui.model.pojos.MovieModel;
import com.skynoff.grabilitytest.ui.utils.GlobalVariables;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailsActivity extends AppCompatActivity {
    MovieModel movieModel;
    @BindView(R.id.cv_movie_title)
    TextView movieTitle;
    @BindView(R.id.cv_movie_release_date)
    TextView movieYear;
    @BindView(R.id.cv_movie_average)
    TextView movieRate;
    @BindView(R.id.cv_movie_popularity)
    TextView moviePopularity;
    @BindView(R.id.movie_description)
    TextView movieDecription;
    @BindView(R.id.cv_movie_language)
    TextView movieLaguage;
    @BindView(R.id.movie_poster)
    ImageView moviePoster;
    @BindView(R.id.detail_back_bt)
    Button backListBt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        try {
            movieModel = new Gson().fromJson(getIntent().getStringExtra("movie"), MovieModel.class);
        } catch (Exception e) {

        }
        movieTitle.setText(movieTitle.getText().toString() + "\n" + movieModel.getTitle());
        movieYear.setText(movieYear.getText().toString() + ": " + movieModel.getReleaseDate());
        movieDecription.setText(movieModel.getOverview());
        movieLaguage.setText(movieLaguage.getText().toString() + ": " + movieModel.getOriginalLanguage());
        movieRate.setText(String.valueOf(movieRate.getText().toString() + ": " + movieModel.getVoteAverage()));
        moviePopularity.setText(moviePopularity.getText().toString() + ": " + String.valueOf(movieModel.getPopularity()));
        Picasso.with(this).load(GlobalVariables.POSTER_BASE_URL + movieModel.getPosterPath()).fit().
                placeholder(this.getResources().getDrawable(R.mipmap.ic_launcher)).
                error(this.getResources().getDrawable(R.mipmap.ic_launcher)).into(moviePoster);

        backListBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        movieTitle = null;
        movieYear = null;
        movieDecription = null;
        movieLaguage = null;
        movieRate = null;
        moviePopularity = null;
    }
}
