package com.skynoff.grabilitytest.ui.model.implementations;

import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import com.skynoff.grabilitytest.R;
import com.skynoff.grabilitytest.background.ws.definitions.ApiPaths;
import com.skynoff.grabilitytest.background.ws.responses.MovieListResponse;
import com.skynoff.grabilitytest.background.ws.responses.UpcomingsResponse;
import com.skynoff.grabilitytest.db.DBManager;
import com.skynoff.grabilitytest.ui.model.pojos.MovieModel;
import com.skynoff.grabilitytest.ui.presenter.callbacks.SplashCallback;
import com.skynoff.grabilitytest.ui.utils.GlobalVariables;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by cesar.smith on 9/29/2017.
 */

public class SplashModel implements SplashCallback.Model {
    private SplashCallback.Presenter presenter;
    private List<MovieModel> upComings;
    private final OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .readTimeout(60, TimeUnit.SECONDS)
            .connectTimeout(60, TimeUnit.SECONDS)
            .build();
    private Retrofit retrofit;

    public SplashModel(SplashCallback.Presenter presenter) {
        this.presenter = presenter;
        upComings = new ArrayList<>();
        initRetrofit();
    }


    @Override
    public void getUpcomings(final Context context) {
        initRetrofit();
        final ApiPaths serverRequest = retrofit.create(ApiPaths.class);
        final Call<UpcomingsResponse> upcomingRequest = serverRequest.getMovieUpcoming(GlobalVariables.API_V3);
        upcomingRequest.enqueue(new Callback<UpcomingsResponse>() {
            @Override
            public void onResponse(@NonNull Call<UpcomingsResponse> call, @NonNull Response<UpcomingsResponse> response) {
                if (response.code() == 200) {

                        upComings = response.body().getResults();
                        presenter.setMainList();

                } else {
                    Log.e("Error code: ", "" + response.code());

                }
            }

            @Override
            public void onFailure(Call<UpcomingsResponse> call, Throwable t) {
                presenter.showError(context.getString(R.string.failed_server));
                Log.e("Splash model", t.getMessage());
            }
        });

    }

    @Override
    public void getMainList(final Context context) {
        initRetrofit();
        final ApiPaths serverRequest = retrofit.create(ApiPaths.class);
        Call<MovieListResponse> listRequest = serverRequest.getMovieList(GlobalVariables.API_V3);
        listRequest.enqueue(new Callback<MovieListResponse>() {
            @Override
            public void onResponse(Call<MovieListResponse> call, Response<MovieListResponse> response) {
                if (response.code() == 200) {
                    MovieListResponse lista = response.body();
                    List<MovieModel> movies = lista.getItems();
                    LoadDb loadDb = new LoadDb(context, lista);
                    loadDb.execute();

                } else {
                    Log.e("Error code: ", "" + response.code());

                }
            }

            @Override
            public void onFailure(Call<MovieListResponse> call, Throwable t) {
                presenter.showError(context.getString(R.string.failed_server));
                Log.e("Splash model", t.getMessage());

            }
        });


    }

    private void initRetrofit() {
        retrofit = new Retrofit.Builder()
                .baseUrl(GlobalVariables.URL_BASE).addConverterFactory(GsonConverterFactory.create()).client(okHttpClient)
                .build();
    }


    private class LoadDb extends AsyncTask<Void, Void, Void> {
        Context context;
        MovieListResponse listaM;
        DBManager dbManager;
        List<MovieModel> movies = new ArrayList<>();

        public LoadDb(Context context, MovieListResponse listaM) {
            this.context = context;
            this.listaM = listaM;
            dbManager = new DBManager(context);
        }

        @Override
        protected void onPreExecute() {
            dbManager.deleteTableMovies();
            dbManager.deleteTableUpcomings();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            movies = dbManager.getMovies();
            presenter.startMainList(movies);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            dbManager.setUpcomingsContainer(upComings);
            dbManager.addMovies(listaM);
            return null;
        }
    }


}
