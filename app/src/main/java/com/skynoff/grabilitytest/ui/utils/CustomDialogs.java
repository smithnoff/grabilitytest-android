package com.skynoff.grabilitytest.ui.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;

import com.skynoff.grabilitytest.R;
import com.skynoff.grabilitytest.ui.view.activities.MovieListActivity;

/**
 * Created by cesar.smith on 10/3/2017.
 */

public class CustomDialogs {

    public static void errorDialog(final Activity activity, String msg, int option) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
        dialog.setTitle(R.string.warning_text);
        dialog.setMessage(msg);
        if (option == 1) {
            dialog.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    activity.finish();
                }
            });
        } else {
            if (option == 2) {
              dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                  @Override
                  public void onClick(DialogInterface dialogInterface, int i) {
                      activity.startActivity(new Intent(activity, MovieListActivity.class));
                      activity.finish();
                  }
              });
              dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                  @Override
                  public void onClick(DialogInterface dialogInterface, int i) {
                      activity.finish();
                  }
              });
            }
        }
        dialog.create().show();


    }


}
