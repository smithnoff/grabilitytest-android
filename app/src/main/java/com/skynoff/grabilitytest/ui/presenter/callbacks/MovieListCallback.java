package com.skynoff.grabilitytest.ui.presenter.callbacks;

import android.content.Context;

import com.skynoff.grabilitytest.ui.model.pojos.MovieModel;

import java.util.List;

/**
 * Created by cesar.smith on 10/1/2017.
 */

public interface MovieListCallback {

    interface view{

        void sortByRating(List<MovieModel> list);
        void sortByPopularity(List<MovieModel> list);
        void sortByUpcoming(List<MovieModel> list);

        void getUpcoming();

    }

    interface presenter{
        void sortByRating(List<MovieModel> list);
        void sortByPopularity(List<MovieModel> list);
        void sortByUpcoming(List<MovieModel> list);
        void getUpcoming(Context context);
        void getPopular(Context context);
        void getTopRated(Context context);



    }
    interface model{
        void getPopular(Context context);
        void getTopRated(Context  context);
        void getUpcoming(Context context);
    }


}
