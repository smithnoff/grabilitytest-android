package com.skynoff.grabilitytest.ui.presenter.callbacks;

import android.content.Context;

import com.skynoff.grabilitytest.ui.model.pojos.MovieModel;

import java.util.List;

/**
 * Created by cesar.smith on 9/29/2017.
 */

public interface SplashCallback {

    interface View{
       void setMainList();
       void startMainList(List<MovieModel>lista);
       void showError(String msg);
    }
    interface Presenter{
        void setMainList();
        void getUpcomings(Context context);
        void getMainList(Context context);
        void startMainList(List<MovieModel>lista);
        void showError(String msg);


    }
    interface Model{
        void getUpcomings(Context context);
        void getMainList(Context context);

    }




}
