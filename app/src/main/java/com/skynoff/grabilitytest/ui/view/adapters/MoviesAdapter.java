package com.skynoff.grabilitytest.ui.view.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.gson.Gson;
import com.skynoff.grabilitytest.R;
import com.skynoff.grabilitytest.ui.model.pojos.MovieModel;
import com.skynoff.grabilitytest.ui.view.activities.DetailsActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cesar.smith on 9/30/2017.
 */

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MoviesViewHolder> implements Filterable {
    List<MovieModel> listMovies = new ArrayList<>();
    List<MovieModel> filterListMovies;
    String basePosterUrl = "http://image.tmdb.org/t/p/w185//";
    Context context;

    public MoviesAdapter(Context context, List<MovieModel> listMovies) {
        this.listMovies = listMovies;
        this.context = context;
        filterListMovies = new ArrayList<>();
        this.filterListMovies.addAll(listMovies);
    }

    @Override
    public MoviesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cv_movie, parent, false);
        return new MoviesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MoviesViewHolder holder, int position) {
        final MovieModel movie = filterListMovies.get(position);
        holder.title.setText(context.getString(R.string.movie_name) + " " + movie.getTitle());
        holder.year.setText(context.getString(R.string.movie_realease_date) + " " + movie.getReleaseDate());
        holder.average.setText(context.getString(R.string.movie_average) + " " + String.valueOf(movie.getVoteAverage()));
        holder.description.setText(context.getString(R.string.movie_description) + "\n" + String.valueOf(movie.getOverview()));
        holder.popularity.setText(context.getString(R.string.movie_popularity) + " " + String.valueOf(movie.getVoteCount()));
        Picasso.with(context).load(basePosterUrl + movie.getPosterPath()).fit().
                placeholder(context.getResources().getDrawable(R.mipmap.ic_launcher)).
                error(context.getResources().getDrawable(R.mipmap.ic_launcher)).into(holder.poster);
        holder.header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String strMovie = (new Gson().toJson(movie));
                context.startActivity(new Intent(context, DetailsActivity.class).putExtra("movie", strMovie));
            }
        });


    }

    @Override
    public int getItemCount() {
        return (null != filterListMovies ? filterListMovies.size() : 0);
    }

    public void filter(final String text) {

        new Thread(new Runnable() {
            @Override
            public void run() {

                filterListMovies.clear();

                if (TextUtils.isEmpty(text)) {

                    filterListMovies.addAll(listMovies);

                } else {
                    for (MovieModel item : listMovies) {
                        if (item.getTitle().toLowerCase().contains(text.toLowerCase())) {
                            filterListMovies.add(item);
                        }
                    }
                }

                ((Activity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        notifyDataSetChanged();
                    }
                });

            }
        }).start();

    }

    private Filter fMovies;

    @Override
    public Filter getFilter() {
        if (fMovies == null) {
            fMovies = new TitleFilter();
        }
        return fMovies;
    }

    private class TitleFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            FilterResults results = new FilterResults();


            if (charSequence == null || charSequence.length() == 0) {
                results.values = listMovies;
                results.count = listMovies.size();

            } else {

                ArrayList<MovieModel> fRecords = new ArrayList<>();

                for (MovieModel movieMatch : listMovies) {
                    if (movieMatch.getTitle().toUpperCase().trim().contains(charSequence.toString().toUpperCase().trim())) {
                        fRecords.add(movieMatch);
                    }
                }
                results.values = fRecords;
                results.count = fRecords.size();
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            listMovies = (ArrayList<MovieModel>) filterResults.values;
            notifyDataSetChanged();
        }
    }


    class MoviesViewHolder extends RecyclerView.ViewHolder {
        ImageView poster;
        TextView title, year, average, description, popularity;
        TableRow header;

        public MoviesViewHolder(View itemView) {
            super(itemView);
            poster = (ImageView) itemView.findViewById(R.id.iv_poster);
            title = (TextView) itemView.findViewById(R.id.cv_movie_title);
            year = (TextView) itemView.findViewById(R.id.cv_movie_release_date);
            average = (TextView) itemView.findViewById(R.id.cv_movie_average);
            description = (TextView) itemView.findViewById(R.id.cv_movie_description);
            popularity = (TextView) itemView.findViewById(R.id.cv_movie_popularity);
            header = (TableRow) itemView.findViewById(R.id.cv_header);
        }
    }

}
