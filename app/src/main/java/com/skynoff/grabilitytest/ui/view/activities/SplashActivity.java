package com.skynoff.grabilitytest.ui.view.activities;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.skynoff.grabilitytest.R;
import com.skynoff.grabilitytest.db.DBManager;
import com.skynoff.grabilitytest.ui.model.pojos.MovieModel;
import com.skynoff.grabilitytest.ui.presenter.callbacks.SplashCallback;
import com.skynoff.grabilitytest.ui.presenter.implementations.SplashPresenter;
import com.skynoff.grabilitytest.ui.utils.CustomDialogs;
import com.skynoff.grabilitytest.ui.utils.GlobalVariables;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashActivity extends AppCompatActivity implements SplashCallback.View {

    @BindView(R.id.texto)
    TextView txtoV;
    @BindView(R.id.progressBarMovies)
    ProgressBar loadingMoviesBar;
    boolean dbexist=false;

    private SplashCallback.Presenter presenter;
    private DBManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

        manager = new DBManager(this);
        if (getConnectivityStatusString()) {
            presenter = new SplashPresenter(this);
            presenter.getUpcomings(this);
        } else {
            if(dbexist)
            CustomDialogs.errorDialog(this, getString(R.string.no_internet_no_db), 1);
           else
                CustomDialogs.errorDialog(this, getString(R.string.no_internet_db_yes), 2);


        }


    }


    @Override
    public void setMainList() {
        presenter.getMainList(this);

    }

    @Override
    public void startMainList(List<MovieModel> lista) {
       // ListTemp.setListMov(lista);
        startActivity(new Intent(this, MovieListActivity.class));
        finish();
    }

    @Override
    public void showError(String msg) {
        CustomDialogs.errorDialog(this, msg, 1);

    }


    public boolean getConnectivityStatusString() {
        boolean status = false;
        final ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected()) {
            status = true;
        } else {
            status = false;

            File dbtest = getApplicationContext().getDatabasePath(GlobalVariables.DB_NAME);
            if (dbtest.exists())
                dbexist = true;
            else
                dbexist = false;

        }
        return status;
    }
}
