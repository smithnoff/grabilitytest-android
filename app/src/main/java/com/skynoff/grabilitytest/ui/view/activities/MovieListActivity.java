package com.skynoff.grabilitytest.ui.view.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.widget.EditText;

import com.skynoff.grabilitytest.R;
import com.skynoff.grabilitytest.db.DBManager;
import com.skynoff.grabilitytest.ui.model.pojos.MovieModel;
import com.skynoff.grabilitytest.ui.presenter.callbacks.MovieListCallback;
import com.skynoff.grabilitytest.ui.presenter.implementations.MovieListPresenter;
import com.skynoff.grabilitytest.ui.view.adapters.MoviesAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieListActivity extends AppCompatActivity implements MovieListCallback.view {

    @BindView(R.id.recycler_movies)
    RecyclerView recyclermovies;
    @BindView(R.id.search_bar)
    EditText searchBar;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            searchBar.setText("");
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    presenter.getPopular(MovieListActivity.this);
                    return true;
                case R.id.navigation_dashboard:
                    presenter.getTopRated(MovieListActivity.this);
                    return true;
                case R.id.navigation_notifications:
                    presenter.getUpcoming(MovieListActivity.this);
                    return true;
            }
            return false;
        }
    };
    private MoviesAdapter adapterMovies;
    private MovieListPresenter presenter;
    DBManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        presenter = new MovieListPresenter(this);
        manager = new DBManager(this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclermovies.setLayoutManager(layoutManager);
        adapterMovies = new MoviesAdapter(this, manager.getMovies());
        recyclermovies.setAdapter(adapterMovies);
        searchBar.addTextChangedListener(watcher);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

    }

    @Override
    public void sortByRating(List<MovieModel> list) {

        setOrderAdapter(list);

    }

    @Override
    public void sortByPopularity(List<MovieModel> list) {

        setOrderAdapter(list);

    }

    @Override
    public void sortByUpcoming(List<MovieModel> list) {
        setOrderAdapter(list);

    }


    @Override
    public void getUpcoming() {

    }

    public void setOrderAdapter(List<MovieModel> list) {
        adapterMovies = new MoviesAdapter(this, list);
        recyclermovies.setAdapter(adapterMovies);
        recyclermovies.getAdapter().notifyDataSetChanged();
    }

    TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            adapterMovies.filter(charSequence.toString());
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };


}
