package com.skynoff.grabilitytest.ui.presenter.implementations;

import android.content.Context;

import com.skynoff.grabilitytest.ui.model.implementations.SplashModel;
import com.skynoff.grabilitytest.ui.model.pojos.MovieModel;
import com.skynoff.grabilitytest.ui.presenter.callbacks.SplashCallback;

import java.util.List;

/**
 * Created by cesar.smith on 9/29/2017.
 */

public class SplashPresenter implements SplashCallback.Presenter {

    private SplashCallback.View view;
    private SplashCallback.Model model;

    public SplashPresenter(SplashCallback.View view) {
        this.view = view;
        model=new SplashModel(SplashPresenter.this) ;
    }


    @Override
    public void setMainList() {
        if(view !=null)
        {
            view.setMainList();
        }
    }




    @Override
    public void getUpcomings(Context context) {
        if(view !=null)
        {
            model.getUpcomings(context);
        }
    }

    @Override
    public void getMainList(Context context) {
        if(view !=null)
        {
            model.getMainList(context);
        }
    }

    @Override
    public void startMainList(List<MovieModel> lista) {
        if(view!=null)
        {
            view.startMainList(lista);
        }
    }

    @Override
    public void showError(String msg) {
        if(view!=null)
        {
            view.showError(msg);
        }
    }
}
