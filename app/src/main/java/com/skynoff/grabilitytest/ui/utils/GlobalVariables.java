package com.skynoff.grabilitytest.ui.utils;

/**
 * Created by cesar.smith on 9/29/2017.
 */

public class GlobalVariables {


    public static final String DB_NAME = "GrabiityMovies";
    public static final String URL_BASE ="https://api.themoviedb.org";
    public static final String API_V3 ="533457a1363f6d2de168de76ca6f8997";
    public static final String POSTER_BASE_URL ="http://image.tmdb.org/t/p/w185//";

  /*  public static final String apiV4="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI1MzM0NTdhMTM2M2Y2ZDJkZTE2OGRlNzZjYTZmODk5NyIsInN1YiI6IjU5Y2ViNGU1YzNhMzY4MWUxNzAwMzA0NyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.esaQNnlVySpSrmWvNTIt8Kt_RQDwipEiAmKl4rGM9bs";
    public static final String exampleRequest="https://api.themoviedb.org/3/movie/550?api_key=533457a1363f6d2de168de76ca6f8997";*/


}
