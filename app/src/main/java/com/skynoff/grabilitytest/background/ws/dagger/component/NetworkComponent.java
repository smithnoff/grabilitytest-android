package com.skynoff.grabilitytest.background.ws.dagger.component;

import com.skynoff.grabilitytest.ui.view.activities.SplashActivity;



/**
 * Created by cesar.smith on 10/3/2017.
 */
/*@Singleton
@Component(modules = {AppModule.class, NetworkModule.class})*/
public interface NetworkComponent {
    void inject(SplashActivity activity);

}
