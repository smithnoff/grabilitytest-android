package com.skynoff.grabilitytest.background.ws.dagger;

import android.app.Application;

import com.skynoff.grabilitytest.background.ws.dagger.component.NetworkComponent;

/**
 * Created by cesar.smith on 10/3/2017.
 */

public class App extends Application {
    private NetworkComponent networkComponent;

    @Override
    public void onCreate() {
        super.onCreate();
      /*  networkComponent = DaggerNetComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetworkModule(GlobalVariables.URL_BASE))
                .build();*/
    }
    public NetworkComponent getNetComponent() {
        return networkComponent;
    }
}
