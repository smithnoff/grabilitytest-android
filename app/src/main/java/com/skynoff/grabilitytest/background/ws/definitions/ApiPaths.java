package com.skynoff.grabilitytest.background.ws.definitions;

import com.skynoff.grabilitytest.background.ws.responses.MovieListResponse;
import com.skynoff.grabilitytest.background.ws.responses.UpcomingsResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by cesar.smith on 9/30/2017.
 */

public interface ApiPaths {


    @GET("/3/list/1")
    Call<MovieListResponse> getMovieList(@Query("api_key") String token);
    @GET("/3/movie/upcoming")
    Call<UpcomingsResponse> getMovieUpcoming(@Query("api_key") String token);



}
